const initialState = {
	login: {
	  loginId: "",
	  password: "",
	  result: ""
	},
	nav: {
	  index: 0,
	  routes: [{key: 'Home', routeName: 'Home'}]
	},
  subjects: {
    subjects: [
      {
        name: 'maths',
        teacher: {
          name: 'Miss Kelly',
          image: '',
        },
        classroom: '5A',
        homework: {
          dueDate: '30th June 2018',
          topic: 'Numbers',
        },
      },
      {
        name: 'science',
        teacher: {
          name: 'Miss Clarkson', 
          image: '',
        },
        classroom: '5B',
        homework: {
          dueDate: '30th June 2018',
          topic: 'Plants',
        },
      },
      {
        name: 'socials',
        teacher: {
          name: 'Miss Fiona',
          image: '',
        },
        classroom: '5C',
        homework: {
          dueDate: '30th June 2018',
          topic: 'Ancient History',
        },
      },
      {
        name: 'english',
        teacher: {
          name: 'Miss Davidson',
          image: '',
        },
        classroom: '5D',
        homework: {
          dueDate: '30th June 2018',
          topic: 'Grammar exercises',
        },
      }
    ] 
  }
}

export default initialState