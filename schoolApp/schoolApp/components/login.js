import React, { Component } from 'react'
import Connect from './connect'
import {
  ImageBackground,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';

class Login extends Component {
  loginApi() {
    //Todo: Need to add call to backend login api here
    //and get the body and status
    //const {body, status} = api.login(this.props);
    let status = 200;
    if(status === 200) {
      this.props.dispatch({
        type: "LOGIN/LOGIN_RESULT",
        result: 200,
      })
      this.props.dispatch({
        type: "NAV/NOW",
        routes: [{key: "Subjects", routeName: "Subjects"}],
      })      
    }
    else {
      this.props.dispatch({
        type: "LOGIN/LOGIN_RESULT",
        result: 400,
      })
    }
  }

	render() {
    let errorMsg = this.props.result===400 
      ? "Login error occurred"
      : "";
    let disabled = this.props.loginId.length===0||this.props.password.length==0;
		return(
      <View style={{width: "100%", height: "100%", alignItems: 'center'}}>
        <ImageBackground
          style={{width: "100%", height: "100%"}}
          source={require('../../images/background.png')}>
        <Text style={{textAlign: 'center', fontSize: 20}}>Please login</Text>
        <TextInput
          style={{width: "100%", fontSize: 20,}}
          placeholder="Login ID"
          value = {this.props.loginId}
          onChangeText={(value) => this.props.dispatch({
            type: "LOGIN/STORE_LOGINID",
            value,
          })}
        />
        <TextInput
          style={{width: "100%", fontSize: 20}}
          placeholder="Password"
          value={this.props.password}
          onChangeText={(value) => this.props.dispatch({
            type: "LOGIN/STORE_PASSWORD",
            value,
          })}
          secureTextEntry={true}
        />
        <TouchableHighlight
          style = {{
            backgroundColor: disabled?'grey':'green',
            width: "100%",
          }}
          onPress={() => this.loginApi()}
          disabled = {disabled}
        >
          <Text style={{
            color: 'white',
            fontSize: 30,
            textAlign: 'center',
          }}>LOGIN</Text>
        </TouchableHighlight>
        <Text>{errorMsg}</Text>
        </ImageBackground>
      </View>
    )
	}
}

export default Connect(state => state.login, Login);