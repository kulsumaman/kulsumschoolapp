import React, { Component } from 'react'
import Connect from './connect'
import { ImageBackground, View } from 'react-native'

export default class Background extends Component {
	render() {
		return(
      <View style={{flex:1}}>
        <ImageBackground
            style={{width: "100%", height: "100%"}}
            source={require('../../images/background.png')}>
        </ImageBackground>
      </View>
    )
  }
}