import React, {Component} from 'react'
import Connect from './connect'
import {Image, TouchableHighlight, Text, View} from 'react-native'

class Subjects extends Component {
  render() {
    const styles = {
      textStyle: {
        fontSize: 20,
        width: "25%",
        borderStyle: 'solid',
        borderColor: 'purple',
        borderWidth: 1,
        fontWeight: 'bold', 
      },
      viewStyle: {
        flexDirection: 'row',
        backgroundColor: '#ffdae0',
      },
      imageStyle: {
        width: "100%", 
        height: "20%",
      }
    }
    return (
      <View style={{width: '100%', height: '100%', backgroundColor: '#ffdae0'}}>
        <Image
          style={styles.imageStyle}
          source={require('../../images/background.png')}/>
        <View style={styles.viewStyle}>
          <Text style={styles.textStyle}>Subject </Text>
          <Text style={styles.textStyle}>Teacher </Text>
          <Text style={styles.textStyle}>Classroom </Text>
          <Text style={styles.textStyle}>Next Homework Submit By</Text>
        </View>
        {this.props.subjects.map((subject,i) => (
          <TouchableHighlight
            key={i}
            onPress={() => (console.log("TBD"))}
          >
            <View style={styles.viewStyle}>
              <Text style={styles.textStyle}>{subject.name} </Text>
              <Text style={styles.textStyle}>{subject.teacher.name} </Text>
              <Text style={styles.textStyle}>{subject.classroom} </Text>
              <Text style={styles.textStyle}>{subject.homework.dueDate} </Text>
            </View>
          </TouchableHighlight>
        ))}
      </View>
    )
  }
}

export default Connect (state => state.subjects, Subjects)