import React from 'react'
import {connect} from 'react-redux'

export default function Connect(mapStateToProps, component) {
  return connect(mapStateToProps)(component)
}