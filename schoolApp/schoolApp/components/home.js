import React, { Component } from 'react'
import Connect from './connect'
import {
  ImageBackground,
  Text,
  TouchableHighlight,
  View
} from 'react-native';

class Home extends Component {
	render() {
    const styles = {
      textStyle: {
          textAlign: 'center',
          fontSize: 30,
      }
    }
    let displayText = this.props.result!==200
      ? <Text style={styles.textStyle}>Click here to Login</Text>
      : <Text style={styles.textStyle}>Welcome, you have logged in successfully</Text>;
		return(
      <ImageBackground
          style={{width: "100%", height: "100%"}}
          source={require('../../images/background.png')}>
        <TouchableHighlight
          onPress = {() => {
            this.props.dispatch({
              type: "NAV/NOW", 
              routes: [{key: "Login", routeName: "Login"}]
            })
          }}
          disabled={this.props.result===200}
        >
          <View style={{width: "100%", height: "50%", alignItems: 'center'}}>
            {displayText}
          </View>
        </TouchableHighlight>
      </ImageBackground>
    );
	}
}

export default Connect(state => state.login, Home);
