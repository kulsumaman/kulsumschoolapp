import initialState from '../initial-state'

export default function reducer(state=initialState.subjects, action) {
  switch(action.type) {
  	default:
  	  return state
  }
}