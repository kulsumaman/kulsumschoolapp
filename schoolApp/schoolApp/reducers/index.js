import { combineReducers } from 'redux'

import nav from './nav'
import login from './login'
import subjects from './subjects'

export default combineReducers({
	nav,
	login,
	subjects,
})