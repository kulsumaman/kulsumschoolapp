import initialState from '../initial-state'

export default function reducer(state=initialState.nav, action) {
	switch(action.type) {
		case "NAV/NOW":
  		return {
  			...state,
  			index: action.routes.length - 1,
  			routes: action.routes.map(n => ({key: n.key, routeName: n.routeName})),
  		}
  	default:
      return state
	}
}