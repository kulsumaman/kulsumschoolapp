import initialState from '../initial-state'

export default function reducer(state=initialState.login, action) {
  switch(action.type) {
  	case 'LOGIN/STORE_LOGINID':
  	  return {
  	  	...state,
  	  	loginId: action.value,
  	  }
  	case 'LOGIN/STORE_PASSWORD':
  	  return {
  	  	...state,
  	    password: action.value,
  	  }
    case 'LOGIN/LOGIN_RESULT':
      return {
        ...state,
        result: action.result,
      }
  	default:
  	  return state
  }
}