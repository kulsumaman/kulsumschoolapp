import React, { Component } from 'react'
import Connect from './components/connect'
import { createDrawerNavigator, createStackNavigator, addNavigationHelpers } from 'react-navigation'
import { initializeListeners } from 'react-navigation-redux-helpers'
import Home from './components/home'
import Login from './components/login'
import Subjects from './components/subjects'

import {
  createReactNavigationReduxMiddleware,
  createReduxBoundAddListener,
} from 'react-navigation-redux-helpers';

const Navigator = createStackNavigator({
  Home: { screen: Home },
  Login: { screen: Login },
  Subjects: { screen: Subjects },
})

class Navigate extends Component {
  // componentWillMount() {
  //   initializeListeners('root', this.props.nav);
  // }
  render() {
    const {dispatch, nav} = this.props
    const navMiddleware = createReactNavigationReduxMiddleware(
      'root',
      state => this.props.nav,
    );
    const addListener = createReduxBoundAddListener('root');
    return (
      <Navigator navigation={{
        dispatch: dispatch,
        state: nav,
        addListener: addListener,
      }} />
    )
  }
}

export default Connect(state => ({nav: state.nav}), Navigate)