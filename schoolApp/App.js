import React, { Component } from 'react'
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux'
import {createLogger} from 'redux-logger'
import reducers from './schoolApp/reducers'

import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Routes from './schoolApp/routes'

const logger = createLogger({ predicate: (getState, action) => __DEV__ })

const store = createStore(
  reducers,
  applyMiddleware(logger),
)

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={{flex: 1}}>
          <Routes />
        </View>
      </Provider>
    );
  }
}